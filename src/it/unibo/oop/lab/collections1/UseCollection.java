package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    
    private static final int ELEMS = 100000;
    private static final int TIMES = 100000;
    
    
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	List<Integer> arrayList = new ArrayList<>();
    	for(int i = 1000; i < 2000; i++) {
    		arrayList.add(i);
    	}
    	
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	List<Integer> list = new LinkedList<>(arrayList);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int lastElementPosition = arrayList.size()-1;
    	Integer last = arrayList.get(lastElementPosition);
    	arrayList.set(lastElementPosition,arrayList.get(0));
    	arrayList.set(0, last);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    
    	
//    	for(Integer elem : arrayList) {
//    		System.out.print(elem + " " );
//    	}
//    	
//    	System.out.println();
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for(int i = 0; i <= ELEMS; i++) {
    		arrayList.add(0, i);
    	}
    	
    	time = System.nanoTime() - time;
    	
    	System.out.println("Insterting " + ELEMS 
    			+ " as first element of arraylist took " + time + "ns" );
    	
    	time = System.nanoTime();
    	for(int i = 0; i <= ELEMS; i++) {
    		list.add(0,i);
    	}
    	
    	time = System.nanoTime() - time;
    	System.out.println("Insterting " + ELEMS 
    			+ " as first element of list took " + time + "ns" );
    	
    	
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
    	time = System.nanoTime();
    	for(int i = 0; i <= TIMES; i++) {
    		arrayList.get(arrayList.size()/2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Reading " + TIMES 
    			+ " times a middle element of arraylist took " + time + "ns" );
    	
    	time = System.nanoTime();
    	for(int i = 0; i <= TIMES; i++) {
    		list.get(list.size()/2);
    	}
    	time = System.nanoTime() - time;
    	System.out.println("Reading " + TIMES 
    			+ " times a middle element of list took " + time + "ns" );
    	
    	
    	
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
    	
    	Map<String, Long> world = new HashMap<>();
    	world.put("Africa", 1_110_635_000l);
    	world.put("Americas", 972_005_000l);
    	world.put("Antarctica", 0l);
    	world.put("Asia", 4_298_723_000l);
    	world.put("Europe", 742_452_000l);
    	world.put("Oceania", 38_304_000l);
    	
        /*
         * 8) Compute the population of the world
         */
    	
    	long population = 0; 
    	for(long p : world.values()) {
    		population = population + p;
    	}
    	
    	System.out.println(population);
    	
    	
    }
}
